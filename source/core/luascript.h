#pragma once

#include <lua5.4/lua.hpp>

//Lua functions

int lua_create_level(lua_State *L);
int lua_create_object(lua_State *L);
int lua_add_component(lua_State *L);
int lua_move_to(lua_State *state);

class LuaVM{
	private:
		lua_State* m_pState;
	public:
		void init(const char *filename);
		void callFunction(const char* name);
		void close();
		lua_State* getState();
		
};


